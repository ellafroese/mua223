package PersonVelo;

public class Velo {
    private String farbe;

    private Person person;

    @Override
    public String toString() {
        return "Velo{" +
                "farbe='" + farbe + '\'' +
                ", person=" + person.getFirstname() +
                '}';
    }

    public Velo(String farbe, Person person) {
        this.farbe = farbe;
        this.person = person;
    }













    public String getFarbe() {
        return farbe;
    }

    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }


    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }



}
