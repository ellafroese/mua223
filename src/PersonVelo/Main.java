package PersonVelo;

public class Main {

    public static void main(String[] args) {

        Person hans = new Person();
        Person heidi = new Person();

        Velo velo1 = new Velo("Grün", hans);
        Velo velo2 = new Velo("Blau", hans);
        Velo velo3 = new Velo("Rot", heidi);

        velo1.setPerson(hans);
        velo2.setPerson(hans);
        velo3.setPerson(heidi);

        hans.getVelos().add(velo1);
        hans.getVelos().add(velo2);
        heidi.getVelos().add(velo3);


        System.out.println(hans);
        System.out.println(heidi);



    }
}
