package PersonVelo;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private String firstname;
    private List<Velo> velos;

    public Person()
    {
        velos = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstname='" + firstname + '\'' +
                ", velos=" + velos +
                '}';
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public List<Velo> getVelos() {
        return velos;
    }

    public void setVelos(List<Velo> velos) {
        this.velos = velos;
    }
}
