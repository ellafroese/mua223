package Kreis;

public class Kreis {

    double radius;
    String farbe;
    double positionX;
    double positionY;

    public Kreis(double radius, String farbe, double positionX, double positionY) {
        this.radius = radius;
        this.farbe = farbe;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public double flaecheKreis(){
        double flaeche = 0.0;

        flaeche = Math.PI * Math.pow(radius, 2.0);

        return flaeche;
    }


    public double durchmesserKreis(){
        double durchmesser = 0.0;

        durchmesser = radius * 2;

        return durchmesser;
    }


    public double umfangKreis(){
        double umfang = 0.0;

        umfang = 2 * Math.PI * radius;

        return umfang;
    }













}
