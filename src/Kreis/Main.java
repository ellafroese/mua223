package Kreis;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {

        launch();

    }

    @Override
    public void start(Stage primaryStage) {

        Canvas canvas = new Canvas(300, 250);
        BorderPane border = new BorderPane();
        canvas.widthProperty().bind(border.widthProperty());
        canvas.heightProperty().bind(border.heightProperty());
        border.setCenter(canvas);



        Scene scene = new Scene(border, 300, 250);
        primaryStage.setTitle("Kreise abbilden");
        primaryStage.setScene(scene);

        primaryStage.show();
        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setFill(Color.DARKKHAKI);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        gc.setFill(Color.LAVENDERBLUSH);
        gc.fillOval(100, 100 , 100, 50);

        gc.setFill(Color.AQUA);
        gc.fillOval(140, 200, 100, 100);

    }



}

//Kreis kreisDurchmesser = new Kreis(2, "Grün", 4,5);
//System.out.println("Durchmesser von Kreis mit Radius " + kreisDurchmesser.radius + " ist " + kreisDurchmesser.durchmesserKreis());

//Kreis kreisFlaeche = new Kreis(2, "Grün", 4,5);
//System.out.println("Fläche von Kreis2 mit Radius " + kreisFlaeche.radius + " ist " + kreisFlaeche.flaecheKreis());

//Kreis kreisUmfang = new Kreis(5, "Grün", 4,5);
//System.out.println("Fläche von Kreis3 mit Radius " + kreisUmfang.radius + " ist " + kreisUmfang.umfangKreis());

