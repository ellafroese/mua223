package RepetitionGUI;

import com.sun.xml.internal.ws.model.AbstractWrapperBeanGenerator;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Observable;


public class TicketKaufenGUI extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane border = new BorderPane();

        // Top erstellen
        Text title = new Text("Ticket kaufen");
        title.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        HBox hboxTop = new HBox();
        hboxTop.getChildren().add(title);
        hboxTop.setAlignment(Pos.CENTER);
        border.setTop(hboxTop);


        // middle teil erstellen
        // in einem GRID
        GridPane grid = new GridPane();
        grid.setHgap(5);
        grid.setVgap(5);

        ColumnConstraints column = new ColumnConstraints();
        column.setPercentWidth(50);
        grid.getColumnConstraints().add(column);
        grid.getColumnConstraints().add(column);


        // 0,0
        Text event = new Text("Event");
        grid.add(event, 0, 0);
        GridPane.setValignment(event, VPos.TOP);
        ListView<String> list = new ListView<>();
        ObservableList<String> items =FXCollections.observableArrayList (
                "Kino", "Theater", "Museum", "Konzert");
        list.setItems(items);
        list.setPrefSize(150, 100);
        grid.add(list, 1, 0);

        // 0, 1
        Text name = new Text("Name");
        grid.add(name, 0, 1);
        TextField nameTextfield = new TextField();
        grid.add(nameTextfield, 1, 1);

        // 0, 2
        Text vorname = new Text("Vorname");
        grid.add(vorname, 0, 2);
        TextField vornameTextfield = new TextField();
        grid.add(vornameTextfield, 1, 2);

        Text bemerkung = new Text("Bemerkung");
        grid.add(bemerkung, 0, 3);
        GridPane.setValignment(bemerkung, VPos.TOP);
        TextField bemerkungTextfield = new TextField();
        bemerkungTextfield.setPrefSize(200, 150);
        grid.add(bemerkungTextfield, 1, 3);




        border.setCenter(grid);

        // Bottom erstellen
        Button buttonOk = new Button();
        buttonOk.setText("OK");
        buttonOk.setPrefSize(100, 50);

        buttonOk.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                items.add("hallo");
            }
        });

        Button buttonAbbrechen = new Button();
        buttonAbbrechen.setText("Abbrechen");
        buttonAbbrechen.setPrefSize(100, 50);

        HBox hboxBottom = new HBox();
        hboxBottom.getChildren().add(buttonOk);
        hboxBottom.getChildren().add(buttonAbbrechen);
        hboxBottom.setAlignment(Pos.CENTER_RIGHT);



        border.setBottom(hboxBottom);
        border.setPadding(new Insets(5,20,10,20));



        Scene scene = new Scene(border, 800, 500);
        primaryStage.setTitle("Ticket kaufen");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
