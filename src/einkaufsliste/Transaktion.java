package einkaufsliste;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Transaktion {

    public boolean articleOnlyOnceADay(Item item){

        Connection con = Connector.connecting();

        boolean inDB = false;

        try{
            PreparedStatement querySelect = con.prepareStatement("select artikel from tbl_einkaufsliste where artikel = ? and datum = ?");
            querySelect.setString(1, item.getItemName());
            java.sql.Date sqlDate = new java.sql.Date(item.getDate().getTime());
            querySelect.setDate(2, sqlDate);

            ResultSet selected = querySelect.executeQuery();

            if (selected.next()){

                inDB = true;
            } else {
                inDB = false;

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return inDB;
    }
}
