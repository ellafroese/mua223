package einkaufsliste;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DAO {


    public int inserts(Item item) {
        int rows = 0;

        Connection con = Connector.connecting();

        Transaktion transaction = new Transaktion();
        boolean isTrue = transaction.articleOnlyOnceADay(item);


        if(!isTrue){
            try{
                PreparedStatement queryInsert = con.prepareStatement("insert into tbl_einkaufsliste (artikel, preis, menge, beschreibung, datum) " +
                        " values (?, ?, ?, ?, ?)");

                queryInsert.setString(1, item.getItemName());
                queryInsert.setDouble(2, item.getPriceOfItem());
                queryInsert.setInt(3, item.getAmountOfItem());
                queryInsert.setString(4, item.getDescriptionOfItem());
                java.sql.Date sqlDate = new java.sql.Date(item.getDate().getTime());
                queryInsert.setDate(5, sqlDate);


                rows = queryInsert.executeUpdate();

                System.out.println("Artikel wurde hinzugefügt");


            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            System.out.println("artikel ist bereits in der DB");

        }





        return rows;

    }

}
