package einkaufsliste;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.time.LocalDate;
import java.util.Calendar;

public class View extends Application {

    public void start(Stage primaryStage) {

        GridPane grid = new GridPane();
        BorderPane border = new BorderPane();

        // Titel of "Einkaufsliste"
        Text title = new Text("Einkaufsliste");
        title.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        HBox hboxTop = new HBox();
        hboxTop.getChildren().add(title);
        hboxTop.setAlignment(Pos.CENTER);
        border.setTop(hboxTop);

        HBox hBox = new HBox();
        Text item = new Text("Apfel");

        hBox.getChildren().add(item);

        border.setCenter(grid);


        // Add Button for adding items
        HBox hboxBottom = new HBox();
        TextField newItem = new TextField();
        newItem.setPromptText("Item");
        hboxBottom.getChildren().add(newItem);

        TextField newPrice = new TextField();
        newPrice.setPromptText("Price");
        hboxBottom.getChildren().add(newPrice);

        TextField newAmount = new TextField();
        newAmount.setPromptText("Amount");
        hboxBottom.getChildren().add(newAmount);

        TextField newDescription = new TextField();
        newDescription.setPromptText("Description");
        hboxBottom.getChildren().add(newDescription);

        Button addItemButton = new Button("Add Item");
        hboxBottom.getChildren().add(addItemButton);
        border.setBottom(hboxBottom);

        addItemButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Connection con = Connector.connecting();

                String itemName = newItem.getText();
                double priceOfItem = Double.parseDouble(newPrice.getText());
                int amountOfPrice = Integer.valueOf(newAmount.getText());
                String descriptionOfItem = newDescription.getText();
                Date date = Calendar.getInstance().getTime();


                Item item = new Item(itemName, priceOfItem, amountOfPrice, descriptionOfItem, date);



                DAO dao = new DAO();
                dao.inserts(item);


                try {
                    con.commit();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
        });



        Scene scene = new Scene(border, 550, 350);

        primaryStage.setTitle("Einkaufsliste");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void run(String[] args) {
        launch(args);
    }




}
