package einkaufsliste;

import java.util.Date;
import java.time.LocalDate;

public class Item {

    String itemName;
    double priceOfItem;
    int amountOfItem;
    String descriptionOfItem;
    Date date;

    public Item(String itemName, double priceOfItem, int amountOfItem, String descriptionOfItem, Date date) {
        this.itemName = itemName;
        this.priceOfItem = priceOfItem;
        this.amountOfItem = amountOfItem;
        this.descriptionOfItem = descriptionOfItem;
        this.date = date;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getPriceOfItem() {
        return priceOfItem;
    }

    public void setPriceOfItem(double priceOfItem) {
        this.priceOfItem = priceOfItem;
    }

    public int getAmountOfItem() {
        return amountOfItem;
    }

    public void setAmountOfItem(int amountOfItem) {
        this.amountOfItem = amountOfItem;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescriptionOfItem() {
        return descriptionOfItem;
    }

    public void setDescriptionOfItem(String descriptionOfItem) {
        this.descriptionOfItem = descriptionOfItem;
    }
}
