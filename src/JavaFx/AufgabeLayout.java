package JavaFx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AufgabeLayout extends Application {

    @Override
    public void start(Stage primaryStage) {
        // row 1
        Button btn = new Button();
        btn.setText("B1");
        btn.setPrefSize(100, 20);

        Button btn2 = new Button();
        btn2.setText("B2");
        btn2.setPrefSize(100, 20);

        Button btn3 = new Button();
        btn3.setText("B3");
        btn3.setPrefSize(100, 20);

        // row 2
        Button btn4 = new Button();
        btn4.setText("B4");
        btn4.setPrefSize(100, 20);

        Button btn5 = new Button();
        btn5.setText("B5");
        btn5.setPrefSize(100, 20);

        Button btn6 = new Button();
        btn6.setText("B6");
        btn6.setPrefSize(100, 20);

        // left
        Button btn7 = new Button();
        btn7.setText("B7");
        btn7.setPrefSize(30, 170);

        // middle button
        Button btn8 = new Button();
        btn8.setText("B8");
        btn8.setPrefSize(30, 30);

        Button btn9 = new Button();
        btn9.setText("B9");
        btn9.setPrefSize(30, 30);


        // buttom
        Button btn10 = new Button();
        btn10.setText("B10");
        btn10.setPrefSize(300, 20);

        // buttom 2
        Button btn11 = new Button();
        btn11.setText("B11");
        btn11.setPrefSize(150, 20);

        Button btn12 = new Button();
        btn12.setText("B12");
        btn12.setPrefSize(150, 20);





        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });

        // erstelle eine horizontale schachtel
        // boxen werden nebeneinander angeordnet
        HBox hbox = new HBox();
        hbox.getChildren().add(btn);
        hbox.getChildren().add(btn2);
        hbox.getChildren().add(btn3);

        HBox hbox2 = new HBox();
        hbox2.getChildren().add(btn4);
        hbox2.getChildren().add(btn5);
        hbox2.getChildren().add(btn6);


        VBox vbox = new VBox();
        vbox.getChildren().add(hbox);
        vbox.getChildren().add(hbox2);

        HBox hboxButtom = new HBox();
        hboxButtom.getChildren().add(btn10);

        HBox hboxButtom2 = new HBox();
        hboxButtom2.getChildren().add(btn11);
        hboxButtom2.getChildren().add(btn12);

        VBox vboxButtom = new VBox();
        vboxButtom.getChildren().add(hboxButtom2);
        vboxButtom.getChildren().add(hboxButtom);

        VBox vboxB7 = new VBox();
        vboxB7.getChildren().add(btn7);

        HBox hboxmiddle = new HBox();
        hboxmiddle.getChildren().add(btn8);
        hboxmiddle.getChildren().add(btn9);
        hboxmiddle.setAlignment(Pos.TOP_CENTER);
        hboxmiddle.setSpacing(10);
        hboxmiddle.setPadding(new Insets(5, 5, 0, 5));


        // add to window
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(vbox);
        borderPane.setBottom(vboxButtom);
        borderPane.setLeft(vboxB7);
        borderPane.setCenter(hboxmiddle);



        Scene scene = new Scene(borderPane,300, 250);

        primaryStage.setTitle("Aufgabe Layout");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}