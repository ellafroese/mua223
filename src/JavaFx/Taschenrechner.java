package JavaFx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Taschenrechner extends Application {

    public static String resultField = "0";
    @Override
    public void start(Stage primaryStage) {
        // eingabefeld
        TextField textField = new TextField (resultField);
        HBox eingabeFeld = new HBox();
        eingabeFeld.getChildren().add(textField);

        textField.setPrefSize(400, 50);

        HBox hboxEingabe= new HBox();
        hboxEingabe.getChildren().add(eingabeFeld);



        GridPane grid = new GridPane();


        grid.add(createButton("%"), 0, 0);
        grid.add(createButton("CE"), 1, 0);
        grid.add(createButton("C"), 2, 0);
        grid.add(createButton("Back"), 3, 0);

        grid.add(createButton("1/x"), 0, 1);
        grid.add(createButton("x^2"), 1, 1);
        grid.add(createButton("-2/x"), 2, 1);
        grid.add(createButton("/"), 3, 1);

        grid.add(createButton("7"), 0, 2);
        grid.add(createButton("8"), 1, 2);
        grid.add(createButton("9"), 2, 2);
        grid.add(createButton("X"), 3, 2);

        grid.add(createButton("4"), 0, 3);
        grid.add(createButton("5"), 1, 3);
        grid.add(createButton("6"), 2, 3);
        grid.add(createButton("-"), 3, 3);

        grid.add(createButton("1"), 0, 4);
        grid.add(createButton("2"), 1, 4);
        grid.add(createButton("3"), 2, 4);
        grid.add(createButton("+"), 3, 4);

        grid.add(createButton("+/-"), 0, 5);
        grid.add(createButton("0"), 1, 5);
        grid.add(createButton("."), 2, 5);
        grid.add(createButton("="), 3, 5);






        BorderPane border = new BorderPane();
        border.setTop(hboxEingabe);
        border.setBottom(grid);


        Scene scene = new Scene(border, 300, 350);

        primaryStage.setTitle("Taschenrechner");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private static Button createButton(String text) {

        Button button = new Button();
        button.setMaxWidth(Double.MAX_VALUE);
        button.setText(text);

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                TaschenrechnerCalculate calculator = new TaschenrechnerCalculate();
                String text = button.getText();
                calculator.calcualate(text);
                System.out.println(calculator.result);
            }
        });

        return button;
    }


    public static void main(String[] args) {

        launch(args);
    }
}