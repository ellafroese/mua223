package transaktionRepetition;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class KontoDAO {

    private static Connection con = null;

    public boolean bezug(double betragAbheben, int kontoNr){

        boolean isTrue = false;

        DBConnection con = new DBConnection();
        Connection connection = con.connecting();


        // select
        try{
            PreparedStatement querySelect = connection.prepareStatement("select kontostand from tbl_konto where kontoNr = ?");
            querySelect.setInt(1, kontoNr);

            ResultSet selected = querySelect.executeQuery();


            if (selected.next()){
                double betragAufKonto = selected.getDouble("kontostand");

                System.out.println("Betrag auf Konto (NR: " + kontoNr + "): " + betragAufKonto);
                System.out.println("Abzuhebender Betrag: " + betragAbheben);

                if (betragAufKonto - betragAbheben >= 0){
                    // genügend Geld --> Betrag abheben (Update machen)
                    PreparedStatement queryUpdate = connection.prepareStatement("update tbl_konto " +
                            "set kontostand = ? where kontoNr = ?");
                    queryUpdate.setDouble(1, (betragAufKonto - betragAbheben));
                    queryUpdate.setInt(2, kontoNr);

                    int anz = queryUpdate.executeUpdate();

                    if(anz == 1){
                        isTrue = true;
                        connection.commit();
                    } else {
                        connection.rollback();
                    }

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        // kann geld abgehoben werden?
        return isTrue;
    }
}
