package GUIUeben;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ListViewGUI extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {

        HBox title = new HBox();
        Text event = new Text("ListView");
        title.getChildren().add(event);

        ListView<String> list = new ListView<String>();
        ObservableList<String> items = FXCollections.observableArrayList (
                "Single", "Double", "Suite", "Family App");
        list.setItems(items);






        BorderPane border = new BorderPane();

        border.setTop(title);
        border.setCenter(list);

        Scene scene = new Scene(border, 300, 200);
        primaryStage.setTitle("Ticket kaufen");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
