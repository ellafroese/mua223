package GUIUeben;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TableViewGUI extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Text event = new Text("Table View");


        TableView table = new TableView();
        table.setPrefSize(500, 200);


        TableColumn firstNameCol = new TableColumn("First Name");
        TableColumn lastNameCol = new TableColumn("Last Name");
        TableColumn emailCol = new TableColumn("Email");


        table.getColumns().addAll(firstNameCol, lastNameCol, emailCol);



        VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 15, 5, 10));
        vbox.getChildren().addAll(table);

        BorderPane border = new BorderPane();
        border.setTop(event);
        border.setCenter(vbox);

        Scene scene = new Scene(border, 550, 250);
        primaryStage.setTitle("Table View");
        primaryStage.setScene(scene);
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
