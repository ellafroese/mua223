package GUIUeben;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connector {


    public static Connection con = null;

    public static Connection connecting(){

        try{
            if (con == null || con.isClosed()){
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/teilnehmer", "root", "");
                con.setAutoCommit(false);
            }

        } catch (Exception e){
            e.printStackTrace();
        }


        return con;
    }
}
