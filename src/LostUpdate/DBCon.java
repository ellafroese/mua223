package LostUpdate;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBCon {

    private static Connection con = null;

    public Connection connecting(){
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/m223", "root", "");
            con.setAutoCommit(false);
            System.out.println("Open: " + !con.isClosed());
        }

        catch (Exception e) {
            e.printStackTrace();
        }


        return con;
    }

}
