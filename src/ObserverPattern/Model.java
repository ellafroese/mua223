package ObserverPattern;

import java.util.Observable;

public class Model extends Observable {

    int number = 0;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;

        this.setChanged();
        this.notifyObservers();
    }
}
