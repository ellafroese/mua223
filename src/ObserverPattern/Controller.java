package ObserverPattern;

import javafx.scene.control.Label;
import javafx.scene.text.Text;

public class Controller {

    private Model model = new Model();

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public void addOne() {

        model.setNumber(model.getNumber() + 1);



    }
}
