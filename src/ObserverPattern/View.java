package ObserverPattern;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Observable;
import java.util.Observer;

public class View extends Application implements Observer {
    private Model model = new Model();
    private Controller controller = new Controller();

    Label number;

    @Override
    public void start(Stage primaryStage) {
        controller.setModel(model);

        model.addObserver(this);

        number = new Label(String.valueOf(model.getNumber()));
        number.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        HBox hboxLabel = new HBox();
        hboxLabel.getChildren().add(number);
        hboxLabel.setAlignment(Pos.TOP_CENTER);
        Button btn = new Button();
        btn.setAlignment(Pos.BOTTOM_CENTER);
        btn.setText("Klick mich");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                controller.addOne();
            }
        });

        StackPane root = new StackPane();
        root.getChildren().add(hboxLabel);
        root.getChildren().add(btn);


        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Observer Pattern");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(model.getNumber());
        number.setText(String.valueOf(model.getNumber()));
    }
}
