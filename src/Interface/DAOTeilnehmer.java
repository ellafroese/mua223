package Interface;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DAOTeilnehmer implements ITeilnehmerDAO{

    @Override
    public int insert(Teilnehmer t) {

        int anzRows = 0;

        Connection con = Connector.connecting();

        try {
            PreparedStatement queryInsert = con.prepareStatement("insert into tbl_teilnehmer (name, date) " +
                    " values (?, ?)");

            queryInsert.setString(1, t.getName());
            java.sql.Date sqlDate = new java.sql.Date(t.getDate().getTime());
            queryInsert.setDate(2, sqlDate);


            anzRows = queryInsert.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }



        return anzRows;
    }

    @Override
    public int delete(Teilnehmer t) {
        return 0;
    }

    @Override
    public int update(Teilnehmer t) {
        return 0;
    }
}
