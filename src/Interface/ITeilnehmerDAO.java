package Interface;

public interface ITeilnehmerDAO {

    public int insert(Teilnehmer t);

    public int delete(Teilnehmer t);

    public int update(Teilnehmer t);

}
