package Interface;

public class TeilnehmerDAOFactory {
    private static double version = 2.0;

    public static ITeilnehmerDAO createTeilnehmerDAO(){

        if (version >= 2) {
            return new DAOTeilnehmer();
        } else {
            return new VerbesserteDAOTeilnehmer();
        }

    }
}
