package Interface;

import einkaufsliste.Connector;
import einkaufsliste.DAO;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

public class ViewTeilnehmer extends Application {

    public void start(Stage primaryStage) {

        GridPane grid = new GridPane();
        BorderPane border = new BorderPane();

        // Titel of "Teilnehmer"
        Text title = new Text("Teilnehmer");
        title.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        HBox hboxTop = new HBox();
        hboxTop.getChildren().add(title);
        hboxTop.setAlignment(Pos.CENTER);
        border.setTop(hboxTop);

        border.setCenter(grid);


        // Add Button for adding Teilnehmer
        HBox hboxBottom = new HBox();
        TextField newTeilnehmer = new TextField();
        newTeilnehmer.setPromptText("Teilnehmer");
        hboxBottom.getChildren().add(newTeilnehmer);

        Button addTeilnehmerButton = new Button("Add Teilnehmer");
        hboxBottom.getChildren().add(addTeilnehmerButton);
        border.setBottom(hboxBottom);

        addTeilnehmerButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Connection con = Connector.connecting();

                String teilnehmerName = newTeilnehmer.getText();
                Date date = Calendar.getInstance().getTime();


                Teilnehmer teilnehmer = new Teilnehmer(teilnehmerName, date);



                DAOTeilnehmer dao = new DAOTeilnehmer();
                dao.insert(teilnehmer);


                try {
                    con.commit();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
        });



        Scene scene = new Scene(border, 550, 350);

        primaryStage.setTitle("Teilnehmer");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }



}
