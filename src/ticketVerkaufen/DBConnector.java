package ticketVerkaufen;

import java.sql.*;
import java.util.Properties;

public class DBConnector {

    private static Connection con = null;

    public static void conneting() {


        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/m223", "root", "");
            System.out.println("Open: " + !con.isClosed());

        } catch (Exception e) {
            e.printStackTrace();

        }

    }



    public static Connection getConnection()  {
        System.out.println("My Connection " + con);

        return con;
    }

    public static int insertPerson(Person p) {
        int anzRows = 0;

        if(personExists(p) == true){
            System.out.println("person ist bereits eingetragen");
        } else{
            System.out.println("Person noch nicht vorhanden --> wurde in DB eingefügt");


            try{
                PreparedStatement query = con.prepareStatement("insert into tbl_person (firstname, lastname, birthdate, legalAge, tickets, price)" +
                        " values ( ?, ?, ?, ?, ?, ?)");
                query.setString(1, p.getVorname());
                query.setString(2, p.getName());
                query.setDate(3, java.sql.Date.valueOf(p.getGeburtsdatum()));
                query.setBoolean(4, p.getVolljaerig());
                query.setInt(5, p.getAnzTickets());
                query.setInt(6, p.getPreis());

                anzRows = query.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
            }



        };


        return anzRows;

    }


    public static boolean personExists(Person p) {

        try{
            PreparedStatement query = con.prepareStatement("select firstname, lastname, birthdate, legalAge, tickets, price from tbl_person");
            System.out.println("Mein select query: " + query);

            ResultSet selected = query.executeQuery();
            while (selected.next()){
                String firstnameNow = selected.getString("firstname");
                System.out.println(firstnameNow);

                if (firstnameNow.equals(p.getVorname())){
                    System.out.println("Name vorhanden");
                    return true;
                } else {
                    continue;
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
