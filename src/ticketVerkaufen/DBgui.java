package ticketVerkaufen;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.Date;

public class DBgui extends Application {

    @Override
    public void start(Stage primaryStage) {

        Label vornameLabel = new Label("Vorname");
        TextField textFieldVorname = new TextField();

        Label nachnameLabel = new Label("Nachname");
        TextField textFieldNachname = new TextField();

        Label geburtsdatumLabel = new Label("Geburtsdatum");
        DatePicker textFieldGeburtsdatum = new DatePicker();

        Label plus18Label = new Label("18+");
        CheckBox textFieldPlus18 = new CheckBox();

        Label anzTicketsLabel = new Label("Anzahl Tickets");
        Spinner<Integer> textFieldAnzTickets = new Spinner<Integer>(1,10,1);

        Label preisLabel = new Label("Preis");
        Spinner<Integer> textFieldPreis = new Spinner<Integer>(5,1000,5);


        Button btnAdd = new Button();
        btnAdd.setText("Add");

        GridPane grid = new GridPane();
        grid.setPrefWidth(800);

        grid.add(vornameLabel, 0, 0);
        grid.add(textFieldVorname, 1, 0);
        grid.add(nachnameLabel, 0, 1);
        grid.add(textFieldNachname, 1, 1);
        grid.add(geburtsdatumLabel, 0, 2);
        grid.add(textFieldGeburtsdatum, 1, 2);
        grid.add(plus18Label, 0, 3);
        grid.add(textFieldPlus18, 1, 3);
        grid.add(anzTicketsLabel, 0, 4);
        grid.add(textFieldAnzTickets, 1, 4);
        grid.add(preisLabel, 0, 5);
        grid.add(textFieldPreis, 1, 5);
        grid.add(btnAdd, 0, 6);






        btnAdd.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");

                Person p = new Person (
                    textFieldVorname.getText(),
                    textFieldNachname.getText(),
                    textFieldGeburtsdatum.getValue(),
                    textFieldPlus18.isSelected(),
                    textFieldAnzTickets.getValue(),
                    textFieldPreis.getValue()
                );


                DBConnector.insertPerson(p);



                //System.out.println(p.toString());



            }
        });



        Scene scene = new Scene(grid, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        DBConnector.conneting();
        launch(args);

        try {
            DBConnector.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
