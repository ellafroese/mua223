package ticketVerkaufen;

import java.time.LocalDate;
import java.util.Date;

public class Person {

    private int id;
    private String name;
    private String vorname;
    private LocalDate geburtsdatum;
    private Boolean volljaerig;
    private int anzTickets;
    private int preis;

    public Person(String name, String vorname, LocalDate geburtsdatum, Boolean volljaerig, int anzTickets, int preis) {
        this.name = name;
        this.vorname = vorname;
        this.geburtsdatum = geburtsdatum;
        this.volljaerig = volljaerig;
        this.anzTickets = anzTickets;
        this.preis = preis;
    }

    // getter methode
    public  int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getVorname() {
        return vorname;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public Boolean getVolljaerig() {
        return volljaerig;
    }

    public int getAnzTickets() {
        return anzTickets;
    }

    public int getPreis() {
        return preis;
    }

    // setter methode
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public void setVolljaerig(Boolean volljaerig) {
        this.volljaerig = volljaerig;
    }

    public void setAnzTickets(int anzTickets) {
        this.anzTickets = anzTickets;
    }

    public void setPreis(int preis) {
        this.preis = preis;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", vorname='" + vorname + '\'' +
                ", geburtsdatum=" + geburtsdatum +
                ", volljaerig=" + volljaerig +
                ", anzTickets=" + anzTickets +
                ", preis=" + preis +
                '}';
    }
}


