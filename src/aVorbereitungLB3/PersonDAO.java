package aVorbereitungLB3;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Observable;

public class PersonDAO implements IPersonDAO{

    public int insert(Person p) {
        int anzRows = 0;

        // get connection
        Connection con = Connector.connecting();


        try {
            PreparedStatement queryInsert = con.prepareStatement("insert into tbl_vorbereitung (firstname, lastname, age, gebdat, currentdate) " +
                    " values (?, ?, ?, ?, ?)");

            queryInsert.setString(1, p.getVorname());
            queryInsert.setString(2, p.getNachname());
            queryInsert.setInt(3, p.getAlter());
            queryInsert.setDate(4, new java.sql.Date(p.getGeburtsdatum().getTime()));
            java.sql.Date sqlDate = new java.sql.Date(p.getRegistrierungsdatum().getTime());
            queryInsert.setDate(5, sqlDate);



            anzRows = queryInsert.executeUpdate();



        } catch (SQLException e) {
            e.printStackTrace();
        }



        return anzRows;

    }

    @Override
    public int delete(Person p) {
        int anzRows = 0;

        // get connection
        Connection con = Connector.connecting();

        try {
            PreparedStatement queryDelete = con.prepareStatement("delete from tbl_vorbereitung where firstname = ? ");

            queryDelete.setString(1, "Hans123");

            anzRows = queryDelete.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return anzRows;
    }

    @Override
    public int update(Person p) {
        int anzRows = 0;

        // get connection
        Connection con = Connector.connecting();

        try {
            PreparedStatement queryupdate = con.prepareStatement("update tbl_vorbereitung set firstname = ?, lastname = ?, age = ?, gebdat = ?, currentdate = ? " +
                    " where firstname = ? ");

            queryupdate.setString(1, p.getVorname());
            queryupdate.setString(2, p.getNachname());
            queryupdate.setInt(3, p.getAlter());
            queryupdate.setDate(4, new java.sql.Date(p.getGeburtsdatum().getTime()));
            java.sql.Date sqlDate = new java.sql.Date(p.getRegistrierungsdatum().getTime());
            queryupdate.setDate(5, sqlDate);
            queryupdate.setString(6, "Hans");


            anzRows = queryupdate.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return anzRows;
    }

    public ObservableList<Person>  getPersonList(){

        // get connection
        Connection con = Connector.connecting();

        ObservableList<Person> personen = FXCollections.observableArrayList();

        try {
            PreparedStatement stmt = con.prepareStatement("select firstname, lastname, age, gebdat, currentdate from tbl_vorbereitung");

            ResultSet rs = stmt.executeQuery();

            while(rs.next()){
                Person tempPerson = new Person(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getDate(4),
                        rs.getDate(5));


                int a = rs.getInt(1);
                String b = rs.getString(2);
                Double c = rs.getDouble(3);
                int d = rs.getInt(4);
                String e = rs.getString(5);

                personen.add(tempPerson);


            }



        } catch (SQLException e) {
            e.printStackTrace();
        }


        return personen;

    }
}
