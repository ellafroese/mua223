package aVorbereitungLB3;

import java.util.Date;

public class Person {
    String vorname;
    String nachname;
    int alter;
    Date geburtsdatum;
    Date registrierungsdatum;

    public Person(String vorname, String nachname, int alter, Date geburtsdatum, Date registrierungsdatum) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.alter = alter;
        this.geburtsdatum = geburtsdatum;
        this.registrierungsdatum = registrierungsdatum;
    }




    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public int getAlter() {
        return alter;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }

    public Date getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public Date getRegistrierungsdatum() {
        return registrierungsdatum;
    }

    public void setRegistrierungsdatum(Date registrierungsdatum) {
        this.registrierungsdatum = registrierungsdatum;
    }
}
