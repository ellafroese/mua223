package aVorbereitungLB3;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.sql.Connection;
import java.util.Calendar;
import java.sql.Date;


public class View extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        PersonDAO dao = new PersonDAO();

        System.out.println(Calendar.getInstance().getTime());

        // inizialisieren
        BorderPane border = new BorderPane();
        HBox hBox = new HBox();
        VBox vboxView = new VBox();
        HBox forButtons = new HBox();
        Button addButton = new Button();
        Button removeButton = new Button();
        HBox inputfields = new HBox();

        // create title
        Text eventTitle = new Text("Vorbereitung für Prüfung");
        eventTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        hBox.getChildren().add(eventTitle);
        hBox.setAlignment(Pos.CENTER);


        // create Table view
        TableView table = new TableView();
        table.setPrefSize(500, 200);

        TableColumn firstNameCol = new TableColumn("First Name");
        TableColumn lastNameCol = new TableColumn("Last Name");
        TableColumn ageCol = new TableColumn("Age");
        TableColumn birthdateCol = new TableColumn("Birthdate");

        firstNameCol.setCellFactory(new PropertyValueFactory<Person, String>("firstname"));
        lastNameCol.setCellFactory(new PropertyValueFactory<Person, String>("lastname"));
        lastNameCol.setCellFactory(new PropertyValueFactory<Person, Integer>("age"));
        lastNameCol.setCellFactory(new PropertyValueFactory<Person, Date>("gebdat"));

        table.getColumns().addAll(firstNameCol, lastNameCol, ageCol, birthdateCol);

        final ObservableList<Person> personen = FXCollections.observableArrayList(dao.getPersonList());

        // Add to vbox
        vboxView.setSpacing(5);
        vboxView.setPadding(new Insets(10, 15, 5, 10));
        vboxView.getChildren().addAll(table);

        // add inputfields to window
        TextField inputfirstname = new TextField();
        inputfirstname.setPromptText("Firstname");
        TextField inputlastname = new TextField();
        inputlastname.setPromptText("Lastname");
        TextField inputAge = new TextField();
        inputAge.setPromptText("Age");
        DatePicker inputBirthdate = new DatePicker();

        inputfields.getChildren().add(inputfirstname);
        inputfields.getChildren().add(inputlastname);
        inputfields.getChildren().add(inputAge);
        inputfields.getChildren().add(inputBirthdate);

        vboxView.getChildren().add(inputfields);


        // add Buttons to window
        addButton.setText("Add Person");
        removeButton.setText("Remove Person");
        forButtons.getChildren().add(addButton);
        forButtons.getChildren().add(removeButton);
        forButtons.setAlignment(Pos.CENTER);



        // add zu border
        border.setTop(hBox);
        border.setCenter(vboxView);
        border.setBottom(forButtons);


        Scene scene = new Scene(border, 550, 350);


        // Onclick event
        addButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Connection con = Connector.connecting();


                String vorname = inputfirstname.getText();
                String nachname = inputlastname.getText();
                int alter = 0;
                try{
                    alter = Integer.parseInt(inputAge.getText());
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }

                Date geburtsdatum = Date.valueOf(inputBirthdate.getValue());
                java.util.Date date = Calendar.getInstance().getTime();

                Person p = new Person(vorname, nachname, alter, geburtsdatum, date);


                dao.insert(p);

                dao.delete(p);

            }
        });



        // show scene
        primaryStage.setTitle("Vorbereitung");
        primaryStage.setScene(scene);
        primaryStage.show();


    }





    public static void main(String[] args) {
        launch(args);
    }
}
