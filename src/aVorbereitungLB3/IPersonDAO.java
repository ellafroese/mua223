package aVorbereitungLB3;

import Interface.Teilnehmer;

import java.util.List;

public interface IPersonDAO {

    public int insert(Person p);

    public int delete(Person p);

    public int update(Person p);

    public List<Person> getPersonList();

}
