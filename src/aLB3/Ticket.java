package aLB3;

import java.util.Date;

public class Ticket {
    int ticketNr;
    int filmVorführungsNr;
    int sitzReihe;
    int sitzPlatz;
    String kundenidentifikation;
    Date kaufdatum;
    int bewertung;

    public Ticket(int ticketNr, int filmVorführungsNr, int sitzReihe, int sitzPlatz, String kundenidentifikation, Date kaufdatum, int bewertung) {
        this.ticketNr = ticketNr;
        this.filmVorführungsNr = filmVorführungsNr;
        this.sitzReihe = sitzReihe;
        this.sitzPlatz = sitzPlatz;
        this.kundenidentifikation = kundenidentifikation;
        this.kaufdatum = kaufdatum;
        this.bewertung = bewertung;
    }

    public int getTicketNr() {
        return ticketNr;
    }

    public void setTicketNr(int ticketNr) {
        this.ticketNr = ticketNr;
    }

    public int getFilmVorführungsNr() {
        return filmVorführungsNr;
    }

    public void setFilmVorführungsNr(int filmVorführungsNr) {
        this.filmVorführungsNr = filmVorführungsNr;
    }

    public int getSitzReihe() {
        return sitzReihe;
    }

    public void setSitzReihe(int sitzReihe) {
        this.sitzReihe = sitzReihe;
    }

    public int getSitzPlatz() {
        return sitzPlatz;
    }

    public void setSitzPlatz(int sitzPlatz) {
        this.sitzPlatz = sitzPlatz;
    }

    public String getKundenidentifikation() {
        return kundenidentifikation;
    }

    public void setKundenidentifikation(String kundenidentifikation) {
        this.kundenidentifikation = kundenidentifikation;
    }

    public Date getKaufdatum() {
        return kaufdatum;
    }

    public void setKaufdatum(Date kaufdatum) {
        this.kaufdatum = kaufdatum;
    }

    public int getBewertung() {
        return bewertung;
    }

    public void setBewertung(int bewertung) {
        this.bewertung = bewertung;
    }
}
