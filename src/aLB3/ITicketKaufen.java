package aLB3;

public interface ITicketKaufen {


    public int insert(Ticket t);

    public int delete(Ticket t);

    public int update(Ticket t);
    public int select(Ticket t);
}
