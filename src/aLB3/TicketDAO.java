package aLB3;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketDAO implements ITicketKaufen{


    @Override
    public int insert(Ticket t) {
        int anzRows = 0;


        // get connection
        Connection con = ConnectorLB3.connecting();

        Transaktion transaktion = new Transaktion();
        anzRows = transaktion.insertOptimistisch(t);


        return anzRows;
    }

    @Override
    public int delete(Ticket t) {
        int anzRows = 0;

        // get connection
        Connection con = ConnectorLB3.connecting();

        try {
            PreparedStatement queryDelete = con.prepareStatement("delete from tbl_ticket where ticketNr = ? ");

            queryDelete.setInt(1, 3);

            anzRows = queryDelete.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return anzRows;
    }





    @Override
    public int update(Ticket t) {
        int anzRows = 0;


        // get connection
        Connection con = ConnectorLB3.connecting();

        Transaktion transaktion = new Transaktion();
        anzRows = transaktion.updateOptimistisch(t);


        return anzRows;


    }









    @Override
    public int select(Ticket t) {

        int anzRows = 0;

        // get connection
        Connection con = ConnectorLB3.connecting();

        try{
            PreparedStatement querySelect = con.prepareStatement("select ticketNr, filmVorführungsNr, sitzReihe, sitzPlatz, kundenidentifikation, kaufdatum, bewertung from tbl_ticket where ticketNr = ?");

            querySelect.setInt(1, 12);


            ResultSet selected = querySelect.executeQuery();

            if (selected.next()){

                anzRows = anzRows + 1;
            } else {
                anzRows = anzRows;

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return anzRows;
    }
}
