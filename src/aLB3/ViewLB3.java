package aLB3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class ViewLB3 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        // inizialisieren
        BorderPane border = new BorderPane();
        HBox hBoxtitel = new HBox();

        HBox hboxFilmauswahl = new HBox();
        VBox vboxBewertung = new VBox();
        VBox vboxView = new VBox();


        // create Title
        Text eventTitle = new Text("Kinotickets kaufen");
        eventTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        hBoxtitel.getChildren().add(eventTitle);
        hBoxtitel.setAlignment(Pos.CENTER);


        // Datum wählen
        VBox datumBox = new VBox();

        Text eventDatumTitel = new Text("Datum wählen");
        DatePicker vorstellungsdatum = new DatePicker();
        Button datebestaetigung = new Button("Bestätigen");

        datumBox.getChildren().add(eventDatumTitel);
        datumBox.getChildren().add(vorstellungsdatum);
        datumBox.getChildren().add(datebestaetigung);


        // film wählen
        RadioButton film1 = new RadioButton("Film 1, 13:00 Uhr, Saal 3");
        RadioButton film2 = new RadioButton("Film 1, 13:00 Uhr, Saal 3");
        RadioButton film3 = new RadioButton("Film 1, 13:00 Uhr, Saal 3");

        VBox filmAuswahlBox = new VBox();
        Text eventFilmWahlen = new Text("Film auswählen");
        filmAuswahlBox.getChildren().add(eventFilmWahlen);
        filmAuswahlBox.getChildren().add(film1);
        filmAuswahlBox.getChildren().add(film2);
        filmAuswahlBox.getChildren().add(film3);


        // Platz wählen
        Text eventPlaetze = new Text("Platz wählen");
        VBox vBoxPlatz = new VBox();
        vBoxPlatz.getChildren().add(eventPlaetze);
        TextField reihe = new TextField();
        reihe.setPromptText("Reihe wählen");
        TextField platz = new TextField();
        platz.setPromptText("Platz wählen");

        vBoxPlatz.getChildren().add(reihe);
        vBoxPlatz.getChildren().add(platz);


        // Kunde speichern
        Text eventKunde = new Text("Kundenangaben speichern");

        HBox ichBinKunde = new HBox();
        RadioButton kundennummerButton = new RadioButton();
        Label kundennummer = new Label("Ich habe eine Kunden Nummer");
        TextField kundennummerEingabe = new TextField();
        kundennummerEingabe.setPromptText("Meine Kundennummer");
        ichBinKunde.getChildren().add(kundennummerButton);
        ichBinKunde.getChildren().add(kundennummer);
        ichBinKunde.getChildren().add(kundennummerEingabe);

        VBox kundenSpeichern = new VBox();
        kundenSpeichern.getChildren().add(ichBinKunde);


        Text eventNichtKunde = new Text("Als Gastbenutzer registrieren");

        HBox gastBenutzer = new HBox();
        RadioButton gastButton = new RadioButton();
        Label gastname = new Label("Vor- und Nachnamen speichern");
        TextField gastnameEingabe = new TextField();
        gastnameEingabe.setPromptText("Mein Vor- und Nachname");
        gastBenutzer.getChildren().add(gastButton);
        gastBenutzer.getChildren().add(gastname);
        gastBenutzer.getChildren().add(gastnameEingabe);

        VBox gastSpeichern = new VBox();
        gastSpeichern.getChildren().add(gastBenutzer);


        // AGB lesen

        Text agb = new Text("Ich bestätige die AGB's");
        RadioButton agbBest = new RadioButton();
        Hyperlink link = new Hyperlink();
        link.setText("http://AGB.com");

        VBox agbbox = new VBox();
        agbbox.getChildren().add(agb);
        agbbox.getChildren().add(agbBest);
        agbbox.getChildren().add(link);



        Text eventBewertung = new Text("Bewertung");
        final Slider bewertung = new Slider(0, 5, 5);
        bewertung.setValue(5);
        bewertung.setShowTickLabels(true);
        bewertung.setShowTickMarks(true);
        bewertung.setMinorTickCount(1);
        bewertung.setBlockIncrement(1);
        vboxBewertung.getChildren().add(eventBewertung);
        vboxBewertung.getChildren().add(bewertung);








        vboxView.getChildren().add(datumBox);
        vboxView.getChildren().add(filmAuswahlBox);
        vboxView.getChildren().add(vBoxPlatz);
        vboxView.getChildren().add(kundenSpeichern);
        vboxView.getChildren().add(gastBenutzer);
        vboxView.getChildren().add(agbbox);
        vboxView.getChildren().add(vboxBewertung);



        Button bestauetigung = new Button();
        bestauetigung.setText("Bestätigen");

        vboxView.setPadding(new Insets(10, 20, 5, 20));

        Scene scene = new Scene(border, 850, 650);

        border.setTop(hBoxtitel);
        border.setCenter(vboxView);
        border.setBottom(bestauetigung);

        // show scene
        primaryStage.setTitle("Ticketkauf");
        primaryStage.setScene(scene);
        primaryStage.show();


    }




    public static void main(String[] args) {
        launch(args);
    }


}
