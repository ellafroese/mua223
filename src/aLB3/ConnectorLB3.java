package aLB3;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectorLB3 {

    public static Connection con = null;

    public static Connection connecting() {

        try {
            if (con == null || con.isClosed()) {
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/LB3", "root", "");
                con.setAutoCommit(false);
                con.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return con;
    }


}
