package aLB3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transaktion {


    public int insertOptimistisch(Ticket t) {
        int anzRows = 0;
        int kontrolleRows = 0;


        // get connection
        Connection con = ConnectorLB3.connecting();
        try {
            PreparedStatement queryInsert = con.prepareStatement("insert into tbl_ticket (ticketNr, filmVorführungsNr, sitzReihe, sitzPlatz, kundenidentifikation, kaufdatum, bewertung) " +
                    " values (?, ?, ?, ?, ?, ?, ? )");

            queryInsert.setInt(1, t.getTicketNr());
            queryInsert.setInt(2, t.getFilmVorführungsNr());
            queryInsert.setInt(3, t.getSitzReihe());
            queryInsert.setInt(4, t.getSitzPlatz());
            queryInsert.setString(5, t.getKundenidentifikation());
            java.sql.Date sqlDate = new java.sql.Date(t.getKaufdatum().getTime());
            queryInsert.setDate(6, sqlDate);
            queryInsert.setInt(7, t.getBewertung());



            anzRows = queryInsert.executeUpdate();

            try{
                PreparedStatement querySelect = con.prepareStatement("select ticketNr, filmVorführungsNr, sitzReihe, sitzPlatz, kundenidentifikation, kaufdatum, bewertung from tbl_ticket where sitzReihe = ? and sitzPlatz = ? ");

                querySelect.setInt(1, t.getSitzReihe());
                querySelect.setInt(2, t.getSitzPlatz());

                ResultSet selected = querySelect.executeQuery();

                if (selected.next()){

                    kontrolleRows = kontrolleRows + 1;
                } else {
                    kontrolleRows = kontrolleRows;

                }

                if (kontrolleRows > 1) {
                    con.rollback();
                    System.out.println("in rolback");

                } else {
                    System.out.println("in commit");
                    con.commit();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }




        } catch (SQLException e) {
            e.printStackTrace();
        }





        return kontrolleRows;
    }


    public int updateOptimistisch(Ticket t) {

        int anzRows = 0;
        int kontrolleRows = 0;


        // get connection
        Connection con = ConnectorLB3.connecting();
        try {
            PreparedStatement queryupdate = con.prepareStatement("update tbl_ticket set filmVorführungsNr = ?, sitzReihe = ? , sitzPlatz = ?" +
                    " where ticketNr = ? ");

            queryupdate.setInt(1, 1);
            queryupdate.setInt(2, 10);
            queryupdate.setInt(3, 11);
            queryupdate.setInt(4, 3);


            anzRows = queryupdate.executeUpdate();


            try{
                PreparedStatement querySelect = con.prepareStatement("select ticketNr, filmVorführungsNr, sitzReihe, sitzPlatz, kundenidentifikation, kaufdatum, bewertung from tbl_ticket " +
                        " where sitzReihe = ? and sitzPlatz = ? and filmVorführungsNr = ?");

                querySelect.setInt(1, t.getSitzReihe());
                querySelect.setInt(2, t.getSitzPlatz());
                querySelect.setInt(3, t.getFilmVorführungsNr());

                ResultSet selected = querySelect.executeQuery();

                if (selected.next()){
                    kontrolleRows = kontrolleRows + 1;
                } else {
                    kontrolleRows = kontrolleRows;
                }

                if (kontrolleRows > 1) {
                    con.rollback();
                    System.out.println("in rolback");

                } else {
                    System.out.println("in commit");
                    System.out.println(kontrolleRows);
                    con.commit();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return kontrolleRows;
    }
}
