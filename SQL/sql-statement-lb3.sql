use lb3;

drop table if exists tbl_ticket;

create table tbl_ticket 
	(
    ticketNr int not null auto_increment, 
    filmVorführungsNr int,
    sitzReihe int not null,
    sitzPlatz int not null,
    kundenidentifikation varchar(50),
    kaufdatum Date,
    bewertung int,
    Primary Key (ticketNr)
    );

    
    