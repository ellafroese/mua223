use m223;

drop table if exists tbl_person;

create table tbl_person (
	personenID int NOT NULL auto_increment,
	firstname varchar(64) null,
    lastname varchar(64) null,
    birthdate date null,
    legalAge bit null,
    tickets int null,
    price decimal(10, 2) null,  
    PRIMARY KEY (personenID)
 );