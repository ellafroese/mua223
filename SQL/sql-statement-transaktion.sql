use transaktion;

drop table if exists tbl_konto;

create table tbl_konto (
	kontoNr int not null,
    betrag double (10,2) not null,
    PRIMARY KEY (kontoNr)
);

insert into tbl_konto (kontoNr, betrag)
values (1234, 300);

insert into tbl_konto (kontoNr, betrag)
values (4321, 500);

insert into tbl_konto (kontoNr, betrag)
values (1111, 10000);

insert into tbl_konto (kontoNr, betrag)
values (3333, 800);


select kontoNr, betrag from tbl_konto;