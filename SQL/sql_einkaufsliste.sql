use einkaufsliste;

drop table if exists tbl_einkaufsliste ;

create table tbl_einkaufsliste(
	artikelnr int not null auto_increment,
    artikel varchar(45),
    preis decimal(10, 2),
    menge int,
    beschreibung varchar(45),
    datum datetime,
    primary key (artikelnr)
);


select artikelnr, artikel, preis, menge, beschreibung, datum from tbl_einkaufsliste;

