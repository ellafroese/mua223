package aLB3;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;

import static org.junit.Assert.*;

public class TicketDAOTest {


    // Funktioniert
    @Test
    public void insertOK() {
        Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(3, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);

        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.insert(t);

        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertTrue(returnvalue >= 1);

    }


    // Funktioniert
    @Test
    public void insertNOK() {
        Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(3, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);

        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.insert(t);

        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertTrue(returnvalue <= 1);

    }


    // Funktioniert
    @Test
    public void deleteOK() {Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(3, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);

        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.delete(t);

        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(returnvalue, 1);
    }


    // funktioniert
    @Test
    public void deleteNOK() {Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(12, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);

        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.delete(t);

        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(returnvalue, 0);


    }



    // funktioniert
    // kein rückgabewert, da nichts in DB
    // kann hinzugefügt werden
    @Test
    public void updateOK() {
        Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(3, 1, 5, 42, "2355", Calendar.getInstance().getTime(), 5);


        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.update(t);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(returnvalue, 0);
    }



    // Funktioniert
    // bereits in DB vorhnden
    @Test
    public void updateNOK() {Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(6, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);


        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.update(t);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertTrue(returnvalue >= 1);
    }


    // funktioniert
    @Test
    public void selectOk() {
        Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(3, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);


        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.select(t);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(returnvalue, 1);
    }



    // funktioniert
    // Testdaten in DAO ticketnr == 12
    @Test
    public void selectNOk() {
        Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(13, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);


        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.select(t);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(returnvalue, 0);
    }
}