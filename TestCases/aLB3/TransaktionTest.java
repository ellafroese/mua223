package aLB3;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;

import static org.junit.Assert.*;

public class TransaktionTest {

    @Test
    public void insertOptimistischOK() {

        Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(6, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);


        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.insert(t);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertTrue(returnvalue >= 1);
    }

    @Test
    public void insertOptimistischNOK() {

        Connection con = ConnectorLB3.connecting();
        Ticket t = new Ticket(3, 1, 3, 4, "2355", Calendar.getInstance().getTime(), 5);


        ITicketKaufen dao = new TicketDAO();
        int returnvalue = dao.insert(t);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // insert kann nicht gemacht werden
        assertTrue(returnvalue <= 1);
    }
}