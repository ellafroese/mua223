import org.junit.Before;
import org.junit.Test;
import ticketVerkaufen.DBConnector;
import ticketVerkaufen.Person;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class DBTest {

    private static Connection con;

    @Before
    public void setup() throws SQLException {
        isDBConnected();


    }

    @Test
    public void isDBConnected() throws SQLException{
        DBConnector.conneting();
        con = DBConnector.getConnection();

        assertTrue(con != null);
        assertTrue(!con.isClosed());
    }



    @Test
    public void insertPersoninDB() throws SQLException{

        Person p = new Person("Patrick", "Jurt", LocalDate.now() , false, 5, 100);
        DBConnector.insertPerson(p);

        assertTrue(DBConnector.insertPerson(p) == 1);
    }

    @Test
    public void selectFromDB() throws SQLException{

        Person p1 = new Person("Patrick", "Jurt", LocalDate.now() , false, 5, 100);
        Person p2 = new Person("Patrick", "Jurt", LocalDate.now() , false, 5, 100);

        DBConnector.insertPerson(p1);
        DBConnector.insertPerson(p2);

        assertTrue("Person 1 ist nicht = Person 2", p1 != p2);
    }


}