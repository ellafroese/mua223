package Kreis;

import org.junit.Test;

import static org.junit.Assert.*;

public class KreisTest {

    @Test
    public void flaecheKreisTest() {

        Kreis kreis = new Kreis(2, "Grün", 4, 6);
        //assertEquals(0, Double.compare(kreis.flaecheKreis(), 4 * Math.PI));
        assertTrue(kreis.flaecheKreis() == 12.566370614359172);

    }

    @Test
    public void durchmesserKreisTest() {

        Kreis kreis = new Kreis(2, "Grün", 4, 6);
        assertTrue(kreis.durchmesserKreis() == 4);

    }


    @Test
    public void umfangKreisTest() {

        Kreis kreis = new Kreis(4, "Grün", 4, 6);
        assertEquals(0, Double.compare(kreis.umfangKreis(), 4 * 2 * Math.PI));
        //assertTrue(kreis.umfangKreis() == 31.41592653589793);

    }
}













