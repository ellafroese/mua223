package aVorbereitungLB3;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;

import static org.junit.Assert.*;

public class PersonDAOTest {

    @Test
    public void insert() {

        Connection con = Connector.connecting();
        Person p = new Person("Hans123", "Muster", 6, Calendar.getInstance().getTime(), Calendar.getInstance().getTime());

        IPersonDAO dao = new PersonDAO();
        int returnvalue = dao.insert(p);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assertEquals(returnvalue, 1);

    }

    @Test
    public void delete() {
        Connection con = Connector.connecting();

        Person p = new Person("Hans123", "Muster", 6, Calendar.getInstance().getTime(), Calendar.getInstance().getTime());

        IPersonDAO dao = new PersonDAO();
        int returnvalue = dao.delete(p);


        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        assertEquals(returnvalue, 1);
    }

    @Test
    public void update() {

        Connection con = Connector.connecting();

        Person p = new Person("Hans", "Muster", 6, Calendar.getInstance().getTime(), Calendar.getInstance().getTime());

        IPersonDAO dao = new PersonDAO();
        int returnvalue = dao.update(p);

        assertEquals(returnvalue, 1);
    }
}