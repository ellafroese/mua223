package Interface;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

public class ITeilnehmerDAOTest {

    @Test
    public void insert() {
        Teilnehmer t = new Teilnehmer("hans", Calendar.getInstance().getTime());
        //ITeilnehmerDAO dao = new VerbesserteDAOTeilnehmer();
        ITeilnehmerDAO dao = TeilnehmerDAOFactory.createTeilnehmerDAO();
        int returnvalue = dao.insert(t);

        assertEquals(returnvalue, 1);
    }

    @Test
    public void delete() {
    }

    @Test
    public void update() {
    }
}