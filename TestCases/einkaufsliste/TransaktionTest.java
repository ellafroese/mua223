package einkaufsliste;

import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class TransaktionTest {

    @Test
    public void articleIsInDB() {
        boolean inDB = false;
        Connector con = new Connector();
        Item item = new Item("patrick",  123, 4, "apfelkuchen", Calendar.getInstance().getTime());

        try{
            PreparedStatement querySelect = con.connecting().prepareStatement("select artikel from tbl_einkaufsliste where artikel = ? and datum = ?");
            querySelect.setString(1, "patrick");
            java.sql.Date sqlDate = new java.sql.Date(item.getDate().getTime());
            querySelect.setDate(2, sqlDate);

            ResultSet selected = querySelect.executeQuery();

            if (selected.next()){
                inDB = true;

            } else {
                inDB = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertTrue(inDB);
    }



    @Test
    public void articleIsNotInDB() {
        boolean inDB = false;
        Connector con = new Connector();
        Item item = new Item("Apfel",  123, 4, "apfelkuchen", Calendar.getInstance().getTime());

        try{
            PreparedStatement querySelect = con.connecting().prepareStatement("select artikel from tbl_einkaufsliste where artikel = ? and datum = ?");
            querySelect.setString(1, "patrick");
            java.sql.Date sqlDate = new java.sql.Date(item.getDate().getTime());
            querySelect.setDate(2, sqlDate);

            ResultSet selected = querySelect.executeQuery();

            if (!selected.next()){
                inDB = false;

            } else {
                inDB = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertTrue(inDB);
    }
}