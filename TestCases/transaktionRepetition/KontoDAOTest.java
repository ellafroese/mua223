package transaktionRepetition;

import org.junit.Before;
import org.junit.Test;

import java.sql.PreparedStatement;

import static org.junit.Assert.*;

public class KontoDAOTest {


    @Test
    public void bezugOk() {

        int kontoNr = 1111;
        double betrag = 50;
        KontoDAO kdao = new KontoDAO();

        boolean erfolgreich = kdao.bezug(betrag, kontoNr);
        assertTrue(erfolgreich);
    }


    @Test
    public void bezugInsNegative() {

        int kontoNr = 3417;
        double betrag = 5000;
        KontoDAO kdao = new KontoDAO();

        boolean erfolgreich = kdao.bezug(betrag, kontoNr);
        assertFalse(erfolgreich);
    }

    @Test
    public void bezugAufNull() {

        int kontoNr = 4321;
        double betrag = 500;
        KontoDAO kdao = new KontoDAO();

        boolean erfolgreich = kdao.bezug(betrag, kontoNr);
        assertTrue(erfolgreich);
    }



}